# Change Log - @modern-js/libuild-plugin-swc

This log was last generated on Fri, 24 Feb 2023 07:18:39 GMT and should not be manually modified.

## 0.11.2
Fri, 24 Feb 2023 07:18:39 GMT

_Version update only_

## 0.11.1
Thu, 23 Feb 2023 12:02:24 GMT

_Version update only_

## 0.11.0
Fri, 17 Feb 2023 08:36:10 GMT

_Version update only_

## 0.10.1
Mon, 13 Feb 2023 06:20:23 GMT

_Version update only_

## 0.10.0
Wed, 08 Feb 2023 11:09:00 GMT

### Updates

- add isModule option

## 0.9.2
Wed, 01 Feb 2023 02:52:09 GMT

### Updates

- update swc plugins version to fix node version warning

## 0.9.1
Tue, 31 Jan 2023 09:51:30 GMT

### Updates

- not change the compiler target

## 0.9.0
Mon, 16 Jan 2023 08:35:04 GMT

_Version update only_

## 0.8.0
Thu, 05 Jan 2023 02:12:37 GMT

_Version update only_

## 0.7.6
Wed, 04 Jan 2023 13:09:40 GMT

_Version update only_

## 0.7.5
Tue, 03 Jan 2023 03:12:59 GMT

_Version update only_

## 0.7.4
Tue, 27 Dec 2022 13:42:04 GMT

_Version update only_

## 0.7.3
Wed, 21 Dec 2022 07:52:26 GMT

_Version update only_

## 0.7.2
Sun, 18 Dec 2022 13:57:57 GMT

_Version update only_

## 0.7.1
Wed, 14 Dec 2022 08:51:29 GMT

_Version update only_

## 0.7.0
Wed, 14 Dec 2022 05:15:41 GMT

_Version update only_

## 0.6.1
Fri, 09 Dec 2022 08:39:53 GMT

_Version update only_

## 0.6.0
Tue, 06 Dec 2022 14:39:31 GMT

### Updates

- add es5Plugin, transformPlugin and umdPlugin

